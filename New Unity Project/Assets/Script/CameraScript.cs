﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    GameObject player;
    [SerializeField] float offside, bossBorder1, bossBorder2, stageborder;
    
    public bool bossBattle;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerScript>().gameObject;
        bossBattle = false;
    }

    // Update is called once per frame
    void Update()
    {

        if(player.transform.position.x > transform.position.x + offside)
        {
            transform.position = new Vector3(player.transform.position.x - offside, transform.position.y, transform.position.z);
        }
        else if (player.transform.position.x < transform.position.x - offside)
        {
            transform.position = new Vector3(player.transform.position.x + offside, transform.position.y, transform.position.z);
        }

        if(transform.position.x < stageborder)
        {
            transform.position = new Vector3(stageborder, transform.position.y, transform.position.z);

        }

        if (bossBattle)
        {
            if (transform.position.x < bossBorder1)
            {
                transform.position = new Vector3(bossBorder1, transform.position.y, transform.position.z);

            }

            if (transform.position.x > bossBorder2)
            {
                transform.position = new Vector3(bossBorder2, transform.position.y, transform.position.z);

            }
        }
    }
}
