﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageManager : MonoBehaviour
{
    public bool stageClear;
    public string scenetujuan;
    public int enemies, defeated;


    // Start is called before the first frame update
    void Start()
    {
        stageClear = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(defeated == enemies)
        {
            stageClear = true;
            gotoScene(scenetujuan);
        }

        
    }

    public void gotoScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }


}
