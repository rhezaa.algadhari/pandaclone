﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour
{
    PlayerScript player;
    public float timebetweenbullet, nextbullet;
    public GameObject projectile;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerScript>();
        nextbullet = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButton("ranged")) {

            Debug.Log("shoot");
            if (nextbullet < Time.time)
            {
                nextbullet = Time.time + timebetweenbullet;
                Vector3 rot;
                if (player.faceRight)
                {
                    rot = new Vector3(0, 90, 0);
                }
                else { rot = new Vector3(0, -90, 0); }

                Instantiate(projectile, player.attackPoint.position, Quaternion.Euler(rot));
            }
        }
    }

    
}
