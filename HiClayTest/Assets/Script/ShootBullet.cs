﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBullet : MonoBehaviour
{
    PlayerScript player;
    public float range;
    public int damage;

    Ray shootRay;
    RaycastHit shootHit;
    public LayerMask enemyLayer;
    LineRenderer gunLine;



    // Start is called before the first frame update
    void Awake()
    {
        player = FindObjectOfType<PlayerScript>();
        damage = player.atkDmg;

        gunLine = GetComponent<LineRenderer>();

        shootRay.origin = (transform.position);
        shootRay.direction = transform.forward;
        
        gunLine.SetPosition(0, transform.position);

        if(Physics.Raycast(shootRay, out shootHit, range, enemyLayer))
        {
            
            Enemy enemy = shootHit.collider.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }

            gunLine.SetPosition(1, shootHit.point);

        }
        else
        {
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
