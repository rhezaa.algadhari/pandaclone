﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Enemy : MonoBehaviour
{
    
    GameObject player;
    Rigidbody rb;
    CameraScript cam;
    [SerializeField] Animator anim;
    [SerializeField] SpriteRenderer sr;
    [SerializeField] float knockback, batasAtas, batasBawah, batasKiri, batasKanan;
    [SerializeField] bool faceRight, isDead, isBoss;
    [SerializeField]public int maxHP, currentHP, atkDmg;
    [SerializeField] Slider slider;
    
    StageManager sm;
    

    // Start is called before the first frame update
    void Start()
    {
        sm = FindObjectOfType<StageManager>();
        cam = FindObjectOfType<CameraScript>();
        rb = GetComponent<Rigidbody>();
        player = FindObjectOfType<PlayerScript>().gameObject;
        anim.SetInteger("state", 1);
        currentHP = maxHP;
        isDead = false;
        if (isBoss)
        {
            cam.bossBattle = true;
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (isBoss)
        {
            slider.value = currentHP;

            
        }

        if (!isDead)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, 3 * Time.deltaTime);

            if (player.transform.position.x < transform.position.x)
            {
                sr.flipX = true;
                faceRight = false;
            }
            else
            {
                sr.flipX = false;
                faceRight = true;
            }
        }
        
               

        if(currentHP <= 0)
        {
            StartCoroutine("EnemyDie");
        }


        if (transform.position.z > batasAtas)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, batasAtas);
        }
        if (transform.position.z < batasBawah)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, batasBawah);
        }
        if (transform.position.x < batasKiri)
        {
            transform.position = new Vector3(batasKiri, transform.position.y, transform.position.z);
        }
        if (transform.position.x > batasKanan)
        {
            transform.position = new Vector3(batasKanan, transform.position.y, transform.position.z);
        }
    }

    public void TakeDamage(int dmg)
    {
        Debug.Log("enemy attacked");

        if (faceRight)
        {
            rb.AddForce(new Vector3(-knockback, 0, 0));
        }
        else
        {
            rb.AddForce(new Vector3(knockback, 0, 0));
        }

        currentHP -= dmg;

    }

    

    IEnumerator EnemyDie()
    {
        
        Debug.Log("enemy die");
        isDead = true;
        anim.SetInteger("state", 3);
        yield return new WaitForSeconds(2);
        sm.defeated += 1;
        gameObject.SetActive(false);
    }
    
    IEnumerator finishStage()
    {
        yield return new WaitForSeconds(2);
        sm.stageClear = true;
    }

    IEnumerator attackPlayer()
    {
        anim.SetTrigger("attack");
        player.GetComponent<PlayerScript>().TakeDamage(atkDmg);
        yield return new WaitForSeconds(3);
        StartCoroutine("attackPlayer");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!isDead)
            {
                StartCoroutine("attackPlayer");
            }
            
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!isDead)
            {
                StopCoroutine("attackPlayer");
            }

        }
    }

}
