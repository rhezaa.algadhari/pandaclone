﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public Joystick joystick;
    public LayerMask enemyLayers;
    public GameObject projectile;
    [SerializeField] Slider slider;
    [SerializeField]Animator anim;
    [SerializeField] float speed, horizontalmove, verticalmove, batasAtas, batasBawah, batasKiri, batasKanan, atkRange,timebetweenbullet, nextbullet;
    [SerializeField] SpriteRenderer sr;
    [SerializeField] public Transform attackPoint;
    [SerializeField] public int atkDmg, MaxHP, currentHP;
    Rigidbody rb;
    public bool faceRight, isDead;

    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim.SetInteger("state", 0);
        faceRight = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHP <= 0)
        {
            isDead = true;
            anim.SetInteger("state", 3);
        }

        slider.value = currentHP;

        if (Input.GetKeyDown(KeyCode.F))
        {
            meleeAttack();
        }


        if (!isDead)
        {
            if (joystick.Horizontal >= .2f)
            {
                horizontalmove = joystick.Horizontal * speed;

                if (!faceRight)
                {
                    flip();
                }

            }
            else if (joystick.Horizontal <= -.2f)
            {
                horizontalmove = joystick.Horizontal * speed;
                if (faceRight)
                {
                    flip();
                }
            }
            else
            {
                horizontalmove = 0;

            }


            if (joystick.Vertical >= .2f)
            {
                verticalmove = joystick.Vertical * speed;

            }
            else if (joystick.Vertical <= -.2f)
            {
                verticalmove = joystick.Vertical * speed;
            }
            else
            {
                verticalmove = 0;
            }


            move(horizontalmove, verticalmove);

            animationControl();
        }
        

        if(transform.position.z > batasAtas)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, batasAtas);
        }
        if (transform.position.z < batasBawah)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, batasBawah);
        }
        if (transform.position.x < batasKiri)
        {
            transform.position = new Vector3(batasKiri, transform.position.y, transform.position.z);
        }
        if (transform.position.x > batasKanan)
        {
            transform.position = new Vector3(batasKanan, transform.position.y, transform.position.z);
        }

        

    }

    void move(float horizontal, float vertical)
    {
        rb.velocity = new Vector3(horizontal, 0, vertical);
        
    }

    public void animationControl()
    {
        if (horizontalmove != 0 || verticalmove != 0)
        {
            anim.SetInteger("state", 1);           
        }
        else
        {
            anim.SetInteger("state", 0);
        }

        
    }


    public void meleeAttack()
    {
        //ismelee = true;

        if (!isDead)
        {
            anim.SetTrigger("attack");

            Collider[] hitEnemies = Physics.OverlapSphere(attackPoint.position, atkRange, enemyLayers);

            foreach (Collider enemy in hitEnemies)
            {
                enemy.GetComponent<Enemy>().TakeDamage(atkDmg);
            }
        }
        
    }

    public void TakeDamage(int dmg)
    {
        Debug.Log("enemy attacking");

        if (faceRight)
        {
            rb.AddForce(new Vector3(-200, 0, 0));
        }
        else
        {
            rb.AddForce(new Vector3(200, 0, 0));
        }

        currentHP -= dmg;

    }

    void flip()
    {
        faceRight = !faceRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

   
    public void shot()
    {
        Debug.Log("shoot");
        if (nextbullet < Time.time)
        {
            nextbullet = Time.time + timebetweenbullet;
            Vector3 rot;
            if (faceRight)
            {
                rot = new Vector3(0, 90, 0);
            }
            else { rot = new Vector3(0, -90, 0); }

            Instantiate(projectile, attackPoint.position, Quaternion.Euler(rot));
        }
    }
    
}
